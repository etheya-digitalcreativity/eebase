<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
 <?php echo $form_edit; ?>
	
	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th colspan="2">
					<strong><?=lang($module_name.'_edit')?> </strong><br />
					<?=lang($module_name.'_edit_description')?>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<?=lang($module_name.'_label') ?> 
 				</td>
				<td style='width:50%;'>
 					<input  dir='ltr' type='text' name='label'  value='<?=$view['label']?>' size='90' maxlength='250' />
				</td>
 			</tr>
			<tr class="<?php echo alternator('even', 'odd');?>">
				<td>
					<?=lang($module_name.'_short_name') ?> 
 				</td>
				<td style='width:50%;'>
 					<input  dir='ltr' type='text' name='short_name'  value='<?=$view['short_name']?>' size='90' maxlength='250' />
				</td>
 			</tr>
 			<tr class="<?php echo alternator('even', 'odd');?>">
				<td colspan="2">
					<?=$list?>
				</td>
 			</tr>
		</tbody>
	</table>

	<input type="hidden" value="<?=$view['id']?>" name="id" /> 
	
	<p><input type="submit" name="submit" value="<?=lang('submit')?>" class="submit" /></p>
	
</form>
