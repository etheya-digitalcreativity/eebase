<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php  
		$tab = $module_name. "_tab"; 
?>
<!-- begin right column -->

<div class="ct_top_nav">
	<div class="ct_nav" >
			<?php foreach (array_keys($nav) as $method) :    ?>
					<?php if (!in_array($method, $no_nav)) : ?>
					
				<span class="button"><a class="nav_button<?php if ( ! $this->input->get('method') || $this->input->get('method') == $method) : ?> current<?php endif; ?>" href="<?=BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$module_name.AMP.'method='.$method?>"><?=lang('nav_head_'.$method)?></a></span><?php endif; ?>
			<?php endforeach; ?>
		<div class="clear_both"></div>	
	</div>	
</div>
<div class="clear_left shun"></div>
<?php if ($this->session->flashdata($module_name.'_system_error')) : ?>
	<div id="ct_system_error">
		<h4><?=$this->session->flashdata($module_name.'_system_error')?></h4>
	</div>
<?php endif; ?>
<?php if ($this->session->flashdata($module_name.'_system_message')) : ?>
	<div id="ct_system_error">
		<h4><?=$this->session->flashdata($module_name.'_system_message')?></h4>
	</div>
<?php endif; ?>

<?php if ( ! $extension_enabled) : ?>
	<div id="ct_system_error">
		<h4><?=lang('extension_not_installed')?></h4>
		<?=lang('please')?> <a href="<?=BASE.AMP.'C=addons_extensions'?>"><?=lang('enable')?></a> <?=lang('before_proceeding')?>
	</div>
<?php endif; ?>
<?php if ( ! $module_enabled) : ?>
	<div id="ct_system_error">
		<h4><?=lang('module_not_installed')?></h4>
		<?=lang('please')?> <a href="<?=BASE.AMP.'C=addons_modules'?>"><?=lang('install')?></a> <?=lang('before_proceeding')?>
	</div>
<?php endif; ?>
<?php if ( ! $no_form) : ?>
<?=$form_open?>
<?php endif; ?>
<div id="<?=$module_name?>_settings_content">
	<input type="hidden" name="<?=$module_name?>_tab" value="<?=$tab?>" id="<?=$module_name?>_tab" />
	<?php $orig_view_path = $this->load->_ci_view_path; ?>
	
	<?php foreach ($sections as $section) : ?>
		
 			<?php $this->load->_ci_view_path = (isset($view_paths[$section])) ? $view_paths[$section] : $orig_view_path; ?>
			<h3 class="accordion" data-hash="<?=$section?>"><?=lang($section.'_header')?></h3>
			<div style="padding: 5px 1px;">
				<?=$this->load->view($section, $data, TRUE)?>
			</div>
		
	<?php endforeach; ?>
<?php if ( ! $no_form) : ?>
<p><input type="submit" name="submit" value="<?=lang('submit')?>" class="submit" /></p>
</form>
<?php endif; ?>

